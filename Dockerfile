# @todo Upgrade to latest 16.04.
FROM ubuntu:bionic

MAINTAINER Derrek Croney <derrek.croney@duke.edu>

ARG PHPVERS="7.3"

ENV STI_SCRIPTS_PATH=/usr/libexec/s2i

LABEL io.k8s.description="Platform for serving Drupal PHP apps in Shepherd" \
      io.k8s.display-name="DUL-DST Drupal" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,dul-dst,drupal,php,apache" \
      io.openshift.s2i.scripts-url="image://${STI_SCRIPTS_PATH}"

ENV DEBIAN_FRONTEND noninteractive

# Configured timezone.
ENV TZ=America/New_York
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Ensure UTF-8.
# RUN locale-gen en_US.UTF-8
# ENV LANG       en_US.UTF-8
# ENV LC_ALL     en_US.UTF-8

# Upgrade all currently installed packages and install web server packages.
RUN apt-get update \
&& apt-get -y install software-properties-common  \
&& add-apt-repository -y ppa:ondrej/php && apt-get update \
&& apt-get -y dist-upgrade

RUN apt-get -y install apache2 libapache2-mod-php${PHPVERS} php${PHPVERS}-common php${PHPVERS}-fpm mysql-client php${PHPVERS}-apcu php${PHPVERS}-bcmath php${PHPVERS}-cli php${PHPVERS}-curl php${PHPVERS}-gd php${PHPVERS}-ldap php${PHPVERS}-memcached php${PHPVERS}-mysql php${PHPVERS}-opcache php${PHPVERS}-mbstring php${PHPVERS}-soap php${PHPVERS}-xml php${PHPVERS}-zip

RUN php -v

RUN apt-get -y install git libedit-dev ssmtp wget \
&& apt-get -y remove --purge software-properties-common \
&& apt-get -y autoremove && apt-get -y autoclean && apt-get clean && rm -rf /var/lib/apt/lists /tmp/* /var/tmp/*

# Install Drupal tools: Robo, Drush, Drupal console and Composer.
RUN wget -q https://getcomposer.org/installer -O - | php -- --install-dir=/usr/local/bin --filename=composer

RUN composer global require drush/drush

# Apache config.
COPY ./files/apache2.conf /etc/apache2/apache2.conf

# PHP config.
COPY ./files/php_custom.ini /etc/php/${PHPVERS}/mods-available/php_custom.ini

# Configure apache modules, php modules, logging.
RUN a2enmod rewrite \
&& a2dismod vhost_alias \
&& a2disconf other-vhosts-access-log \
&& a2dissite 000-default \
&& phpenmod -v ALL -s ALL php_custom

# Add /code /shared directories and ensure ownership by web user.
RUN mkdir -p /code /shared && chown www-data:www-data /code /shared \
  && chmod 0777 /code \
  && chmod 0777 /shared

# Add in bootstrap script.
COPY ./files/apache2-foreground /apache2-foreground
RUN chmod +x /apache2-foreground

# Add s2i scripts.
COPY ./s2i/bin ${STI_SCRIPTS_PATH}
RUN chmod +x ${STI_SCRIPTS_PATH}/*
ENV PATH "$PATH:${STI_SCRIPTS_PATH}"

# Web port.
EXPOSE 8080

# Set working directory.
WORKDIR /code

USER 1001

# Start the web server.
CMD ["/apache2-foreground"]
